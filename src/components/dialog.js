import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { TextField } from '@material-ui/core';

export default function FormDialog({open,handleClose,data,onChange,handleFormSubmit}) {
 const {id,firstname,lastname,location,email,dob,education}=data

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{id?"Update user":"Create new user"}</DialogTitle>
        <DialogContent>
         <form>
             <TextField id="firstname" value={firstname} onChange={e=>onChange(e)} placeholder="Enter firstname" label="firstName" variant="outlined" margin="dense" fullWidth />
             <TextField id="lastname" value={lastname} onChange={e=>onChange(e)} placeholder="Enter lastname" label="lastName" variant="outlined" margin="dense" fullWidth />
             <TextField id="location" value={location} onChange={e=>onChange(e)} placeholder="Enter location" label="location" variant="outlined" margin="dense" fullWidth />
             <TextField id="email" value={email} onChange={e=>onChange(e)} placeholder="Enter email" label="Email" variant="outlined" margin="dense" fullWidth />
           <TextField id="dob" value={dob} onChange={e=>onChange(e)} placeholder="Enter Date of birth (dd/mm/yyyy)" label="Date of Birth" variant="outlined" margin="dense" fullWidth />
           <TextField id="education" value={education} onChange={e=>onChange(e)} placeholder="Enter education" label="education" variant="outlined" margin="dense" fullWidth />
     </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary" variant="outlined">
            Cancel
          </Button>
          <Button  color="primary" onClick={()=>handleFormSubmit()} variant="contained">
            {id?"Update":"Submit"}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
